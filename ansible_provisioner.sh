#!/bin/bash

echo "------------------------------------------"
echo  "START - install ansible provioner - "
echo "------------------------------------------"
echo "[1]: Install ansible & utils"
echo "------------------------------------------"

sudo apt update -qq
sudo apt install -qq -y git wget ansible sshpass gnupg2 curl

echo "------------------------------------------"
echo "[2]: Install VirtualBox"
echo "------------------------------------------"

sudo apt install -qq -y virtualbox

echo "------------------------------------------"
echo "[3]: Install Vagrant"
echo "------------------------------------------"

sudo curl -O https://releases.hashicorp.com/vagrant/2.2.9/vagrant_2.2.9_x86_64.deb
sudo apt -qq -y install ./vagrant_2.2.9_x86_64.deb

sudo apt update -qq

echo "------------------------------------------"
echo "[5]: Display versions"
echo "------------------------------------------" 

sudo apt upgrade
sude apt update
ansible --version
vboxmanage --version
vagrant --version
