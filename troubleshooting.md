Troubleshooting lors de l'installation et du provisioning de la VM

# Connexion qui ne fonctionne pas

Attention !! La connexion s'effectue avec un clavier qwerty même après changement du clavier en Azerty.
Ainsi pour taper "matthieu" il faut taper ",qtthieu" sur le clavier en azerty.

# Terminal qui ne s'ouvre pas

Faire le raccourcis clavier CTRL+ALT+F3 pour ouvrir terminal 3 (tty3)
Installer un autre terminal "terminator"

- su
- apt-get update
- apt install terminator

Faire CTRL+ALT+F2 pour permet tty3

# Clavier en qwerty

Pour changer le clavier par défaut il faut aller dans :

- Paramètres -> clavier -> Ajouter le clavier Français (AZERTY) -> Supprimer le clavier Qwerty

# Copier coller et glissé déposer qui ne fonctionnent pas

Périphériques -> Presse-papier partagé -> bidirectionnel
Périphériques -> Glissé-Déposer -> bidirectionnel

Cliquer sur Périphérique -> insérer les CD des additions invités.

Dans un terminal aller à la racine et executer

- cd media/matthieu
- cd [nom du CD]
- ./VBoxLinuxAdditions.run

# Installer vscode

Télécharger sur le site de vscode le packet .deb : https://code.visualstudio.com/download

e placer à l'endroit ou il est stocké sur la machine et exécuter :
- apt install ./filename.deb

# Créer un dossier partagé

Dans VirtualBox aller dans :
- Dossier Partagés -> + 

Entrer le chemin du dossier à partager
Entrer son nom
Entrer son point de montage : 

Cocher la case "Montage automatique" pour activer automatiquement le dossier de partage au démarrage.
Cocher la case Configuration permanente pour que ce dossier soit partagé de manière permanente.

Insérer l’image CD des Additions invité.

Dans un terminal aller à la racine et executer :

- cd media/matthieu
- cd [nom du CD]
- ./VBoxLinuxAdditions.run
- redémarrer la machine virtuelle

Si un message d'erreur (permission denied) s'affiche lors de l'ouverture du dossier partagé :

- Ouvrir un terminal dans la machine virtuelle
- su
- adduser $USER vboxsf
- redémarrer la vm
